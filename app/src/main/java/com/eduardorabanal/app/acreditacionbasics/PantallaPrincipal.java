package com.eduardorabanal.app.acreditacionbasics;

import android.animation.LayoutTransition;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.edurabroj.iscacredita.R;

public class PantallaPrincipal extends AppCompatActivity {
    String correo[] = {"e.rabanal.r@gmail.com"};

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            salir();
            return true;
        }else
            return super.onKeyDown(keyCode, event);
    }

    public void salir(){
        Toast.makeText(getApplicationContext(),
                R.string.salir_mensaje,
                Toast.LENGTH_SHORT).show();
        finish();
        overridePendingTransition(0, R.anim.fade_in);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_principal);

        final ScrollView scroll = findViewById(R.id.scroll);
        final int delay = 300;

        final ImageView imagenFondo = findViewById(R.id.imagen_fondo);

        final LinearLayout layoutISC = findViewById(R.id.layout_ISC);
        final LinearLayout layoutAutoevaluacion = findViewById(R.id.layout_autoevaluacion);
        final LinearLayout layoutUPN = findViewById(R.id.layout_UPN);
        final LinearLayout layoutAcredita = findViewById(R.id.layout_acredita);


        //-----------Seccion ISC
        final Button btnISC = findViewById(R.id.btn_ISC);
        final LinearLayout.LayoutParams lpISC = (LinearLayout.LayoutParams) layoutISC.getLayoutParams();

        final Button btnMision = findViewById(R.id.btn_mision);
        final TextView txtMision = findViewById(R.id.txt_mision);

        final Button btnVision = findViewById(R.id.btn_vision);
        final TextView txtVision = findViewById(R.id.txt_vision);

        final Button btnValores = findViewById(R.id.btn_valores);
        final TextView txtValores = findViewById(R.id.txt_valores);

        final Button btnPlanEstrategico = findViewById(R.id.btn_planEstrategico);
        final TextView txtPlanEstrategico = findViewById(R.id.txt_planEstrategico);

        final Button btnPlanOperativo = findViewById(R.id.btn_planOperativo);
        final TextView txtPlanOperativo = findViewById(R.id.txt_planOperativo);

        final Button btnPerfilEgresado = findViewById(R.id.btn_perfilEgresado);
        final TextView txtPerfilEgresado = findViewById(R.id.txt_perfilEgresado);

        btnISC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!layoutISC.isShown() || layoutUPN.isShown()|| layoutAutoevaluacion.isShown() || layoutAcredita.isShown())
                    imagenFondo.setVisibility(View.GONE);
                else
                    imagenFondo.setVisibility(View.VISIBLE);

                if (layoutISC.isShown()) {
                    btnISC.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_1));
                    layoutISC.setVisibility(View.GONE);
                }else {
                    btnISC.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_2));
                    layoutISC.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Mision ISC

        btnMision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtMision.isShown())
                    txtMision.setVisibility(View.GONE);
                else {
                    txtMision.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getBottom()+lpISC.topMargin+btnMision.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Vision ISC

        btnVision.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtVision.isShown())
                    txtVision.setVisibility(View.GONE);
                else {
                    txtVision.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getBottom()+lpISC.topMargin+btnVision.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Valores

        btnValores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtValores.isShown())
                    txtValores.setVisibility(View.GONE);
                else {
                    txtValores.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getBottom()+lpISC.topMargin+btnValores.getTop());
                        }
                    }, delay);
                }
            }
        });

        //plan estratégico

        btnPlanEstrategico.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPlanEstrategico.isShown())
                    txtPlanEstrategico.setVisibility(View.GONE);
                else {
                    txtPlanEstrategico.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getBottom()+lpISC.topMargin+btnPlanEstrategico.getTop());
                        }
                    }, delay);
                }
            }
        });

        //plan operativo

        btnPlanOperativo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPlanOperativo.isShown())
                    txtPlanOperativo.setVisibility(View.GONE);
                else {
                    txtPlanOperativo.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getBottom()+lpISC.topMargin+btnPlanOperativo.getTop());
                        }
                    }, delay);
                }
            }
        });

        //perfil Egresado

        btnPerfilEgresado.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPerfilEgresado.isShown()) {
                    txtPerfilEgresado.setVisibility(View.GONE);
                    //overridePendingTransition(0, R.anim.fade_in);
                    //scroll.scrollTo(scroll.getScrollX(),scroll.getScrollY());
                }else {
                    txtPerfilEgresado.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnISC.getBottom()+lpISC.topMargin+btnPerfilEgresado.getTop());
                        }
                    }, delay);
                }
            }
        });


        //Sección autoevaluación
        final Button btnAutoevaluacion = findViewById(R.id.btn_autoevaluacion);

        final LinearLayout.LayoutParams lpAutoevaluacion = (LinearLayout.LayoutParams) layoutAutoevaluacion.getLayoutParams();

        final Button btnInvestigacionISC = findViewById(R.id.btn_investigacionISC);
        final TextView txtInvestigacionISC = findViewById(R.id.txt_investigacionISC);

        final Button btnAcompanamiento = findViewById(R.id.btn_acompanamiento);
        final TextView txtAcompanamiento = findViewById(R.id.txt_acompanamiento);

        btnAutoevaluacion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutISC.isShown() || layoutUPN.isShown() || !layoutAutoevaluacion.isShown() || layoutAcredita.isShown())
                    imagenFondo.setVisibility(View.GONE);
                else
                    imagenFondo.setVisibility(View.VISIBLE);

                if (layoutAutoevaluacion.isShown()) {
                    btnAutoevaluacion.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_s1));
                    layoutAutoevaluacion.setVisibility(View.GONE);
                }else {
                    btnAutoevaluacion.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_s2));
                    layoutAutoevaluacion.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnAutoevaluacion.getTop());
                        }
                    }, delay);
                }
            }
        });

        //InvestigacionISC

        btnInvestigacionISC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtInvestigacionISC.isShown())
                    txtInvestigacionISC.setVisibility(View.GONE);
                else {
                    txtInvestigacionISC.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnAutoevaluacion.getBottom()+lpAutoevaluacion.topMargin+btnInvestigacionISC.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Acompañamiento

        btnAcompanamiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtAcompanamiento.isShown())
                    txtAcompanamiento.setVisibility(View.GONE);
                else {
                    txtAcompanamiento.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnAutoevaluacion.getBottom()+lpAutoevaluacion.topMargin+btnAcompanamiento.getTop());
                        }
                    }, delay);
                }
            }
        });


        //-----Seccion UPN


        final Button btnUPN = findViewById(R.id.btn_UPN);

        final LinearLayout.LayoutParams lpUPN = (LinearLayout.LayoutParams) layoutUPN.getLayoutParams();

        final Button btnMisionUPN = findViewById(R.id.btn_misionUPN);
        final TextView txtMisionUPN = findViewById(R.id.txt_misionUPN);

        final Button btnValoresUPN = findViewById(R.id.btn_valoresUPN);
        final TextView txtValoresUPN = findViewById(R.id.txt_valoresUPN);

        final Button btnVisionUPN = findViewById(R.id.btn_visionUPN);
        final TextView txtVisionUPN = findViewById(R.id.txt_visionUPN);

        final Button btnPilares = findViewById(R.id.btn_pilares);
        final TextView txtPilares = findViewById(R.id.txt_pilares);

        final Button btnServicios = findViewById(R.id.btn_servicios);
        final TextView txtServicios = findViewById(R.id.txt_servicios);

        final Button btnModeloEducativo = findViewById(R.id.btn_modeloEducativo);
        final TextView txtModeloEducativo = findViewById(R.id.txt_modeloEducativo);

        btnUPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutISC.isShown() || !layoutUPN.isShown() || layoutAutoevaluacion.isShown() || layoutAcredita.isShown())
                    imagenFondo.setVisibility(View.GONE);
                else
                    imagenFondo.setVisibility(View.VISIBLE);

                if (layoutUPN.isShown()) {
                    btnUPN.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_1));
                    layoutUPN.setVisibility(View.GONE);
                }else {
                    btnUPN.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_2));
                    layoutUPN.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnUPN.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Mision UPN


        btnMisionUPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtMisionUPN.isShown())
                    txtMisionUPN.setVisibility(View.GONE);
                else {
                    txtMisionUPN.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, (btnUPN.getBottom()+lpUPN.topMargin+btnMisionUPN.getTop() ));
                        }
                    }, delay);
                }
            }
        });

        //Mision UPN

        btnValoresUPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtValoresUPN.isShown())
                    txtValoresUPN.setVisibility(View.GONE);
                else {
                    txtValoresUPN.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, (btnUPN.getBottom()+lpUPN.topMargin+btnValoresUPN.getTop() ));
                        }
                    }, delay);
                }
            }
        });



        //Vision UPN

        btnVisionUPN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtVisionUPN.isShown())
                    txtVisionUPN.setVisibility(View.GONE);
                else {
                    txtVisionUPN.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnUPN.getBottom()+lpUPN.topMargin+btnVisionUPN.getTop());
                        }
                    }, delay);
                }
            }
        });

        //pilares upn

        btnPilares.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtPilares.isShown())
                    txtPilares.setVisibility(View.GONE);
                else {
                    txtPilares.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnUPN.getBottom()+lpUPN.topMargin+btnPilares.getTop());
                        }
                    }, delay);
                }
            }
        });

        //servicios upn

        btnServicios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtServicios.isShown())
                    txtServicios.setVisibility(View.GONE);
                else {
                    txtServicios.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnUPN.getBottom()+lpUPN.topMargin+btnServicios.getTop());
                        }
                    }, delay);
                }
            }
        });

        //modelo educativo UPN

        btnModeloEducativo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtModeloEducativo.isShown())
                    txtModeloEducativo.setVisibility(View.GONE);
                else {
                    txtModeloEducativo.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, btnUPN.getBottom()+lpUPN.topMargin+btnModeloEducativo.getTop());
                        }
                    }, delay);
                }
            }
        });

        //-----Seccion ACREDITACIÓN
        final Button btnAcredita = findViewById(R.id.btn_acredita);
        final LinearLayout.LayoutParams lpAcredita = (LinearLayout.LayoutParams) layoutAcredita.getLayoutParams();

        final Button btnAcreditaQueEs = findViewById(R.id.btn_acreditaQueEs);
        final TextView txtAcreditaQueEs = findViewById(R.id.txt_acreditaQueEs);

        btnAcredita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (layoutISC.isShown() || layoutUPN.isShown() || layoutAutoevaluacion.isShown() || !layoutAcredita.isShown())
                    imagenFondo.setVisibility(View.GONE);
                else
                    imagenFondo.setVisibility(View.VISIBLE);

                if (layoutAcredita.isShown()) {
                    btnAcredita.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_1));
                    layoutAcredita.setVisibility(View.GONE);
                }else {
                    btnAcredita.setBackground(getResources().getDrawable(R.drawable.boton_redondeado_2));
                    layoutAcredita.setVisibility(View.VISIBLE);


                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnAcredita.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Que es acreditacion

        btnAcreditaQueEs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtAcreditaQueEs.isShown())
                    txtAcreditaQueEs.setVisibility(View.GONE);
                else {
                    txtAcreditaQueEs.setVisibility(View.VISIBLE);


                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0, (btnAcredita.getBottom()+lpAcredita.topMargin+btnAcreditaQueEs.getTop()));
                        }
                    }, delay);
                }
            }
        });

        //Etapas Acreditacion
        final Button btnAcreditaEtapas = findViewById(R.id.btn_acreditaEtapas);
        final TextView txtAcreditaEtapas = findViewById(R.id.txt_acreditaEtapas);

        btnAcreditaEtapas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtAcreditaEtapas.isShown())
                    txtAcreditaEtapas.setVisibility(View.GONE);
                else {
                    txtAcreditaEtapas.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnAcredita.getBottom()+lpAcredita.topMargin+btnAcreditaEtapas.getTop());
                        }
                    }, delay);
                }
            }
        });
        //Acreditacion Beneficios
        final Button btnAcreditaBeneficios = findViewById(R.id.btn_acreditaBeneficios);
        final TextView txtAcreditaBeneficios = findViewById(R.id.txt_acreditaBeneficios);

        btnAcreditaBeneficios.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtAcreditaBeneficios.isShown())
                    txtAcreditaBeneficios.setVisibility(View.GONE);
                else {
                    txtAcreditaBeneficios.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnAcredita.getBottom()+lpAcredita.topMargin+btnAcreditaBeneficios.getTop());
                        }
                    }, delay);
                }
            }
        });

        //Acreditacion - modelo de calidad
        final Button btnAcreditaModelo = findViewById(R.id.btn_acreditaModelo);
        final TextView txtAcreditaModelo = findViewById(R.id.txt_acreditaModelo);

        btnAcreditaModelo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (txtAcreditaModelo.isShown()) {
                    txtAcreditaModelo.setVisibility(View.GONE);
                }else {
                    txtAcreditaModelo.setVisibility(View.VISIBLE);

                    scroll.postDelayed(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            scroll.smoothScrollTo(0,btnAcredita.getBottom()+lpAcredita.topMargin+btnAcreditaModelo.getTop());
                        }
                    }, delay);
                }
            }
        });

        long duracion = delay;

        LinearLayout layoutScroll = findViewById(R.id.layout_scroll);
        LayoutTransition lt = layoutScroll.getLayoutTransition();
            lt.setDuration(duracion);
        lt.enableTransitionType(LayoutTransition.CHANGING);

        LayoutTransition lt_acredita = layoutAcredita.getLayoutTransition();
            lt_acredita.setDuration(duracion);
        lt_acredita.disableTransitionType(LayoutTransition.DISAPPEARING);
        lt_acredita.disableTransitionType(LayoutTransition.CHANGE_DISAPPEARING);

        LayoutTransition lt_ISC = layoutISC.getLayoutTransition();
            lt_ISC.setDuration(duracion);
        lt_ISC.disableTransitionType(LayoutTransition.DISAPPEARING);
        lt_ISC.disableTransitionType(LayoutTransition.CHANGE_DISAPPEARING);

        LayoutTransition lt_UPN = layoutUPN.getLayoutTransition();
            lt_UPN.setDuration(duracion);
        lt_UPN.disableTransitionType(LayoutTransition.DISAPPEARING);
        lt_UPN.disableTransitionType(LayoutTransition.CHANGE_DISAPPEARING);

        LayoutTransition lt_autoevaluacion = layoutAutoevaluacion.getLayoutTransition();
            lt_autoevaluacion.setDuration(duracion);
        lt_autoevaluacion.disableTransitionType(LayoutTransition.DISAPPEARING);
        lt_autoevaluacion.disableTransitionType(LayoutTransition.CHANGE_DISAPPEARING);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_btn_acercaDe:
                acercaDe();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    public void acercaDe() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.acerca_de_titulo);

        Resources res = getResources();
        String mensaje = res.getString(R.string.acerca_de_mensaje);
        String stringConVersion = String.format(mensaje, obtenerNombreVersionActual());
        CharSequence stringConVersion2 = Html.fromHtml(stringConVersion);
        builder.setMessage(stringConVersion2);

        builder.setPositiveButton(R.string.acerca_de_btnAceptar, null);
        builder.setNegativeButton("Enviar e-mail", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                enviarEmail();
            }
        });
        Dialog dialog = builder.create();
        dialog.show();
    }

    public void enviarEmail(){
        composeEmail(correo, "Sobre: ISC Acredita (" + obtenerNombreVersionActual() + ")");
    }

    public void composeEmail(String[] addresses, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    private String obtenerNombreVersionActual() {
        Context context = getApplicationContext();
        PackageInfo pckginfo = null;
        try {
            pckginfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pckginfo.versionName;
    }
}
